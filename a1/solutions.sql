[1]
--a. All artist that has d in its name
SELECT * FROM artists WHERE name LIKE "%d%";

--b. All songs that has a length of less than 230
SELECT * FROM songs WHERE length < 230;

--c. Join the 'albums' and 'songs' tables [album name, song name, song length]
SELECT albums.album_title, songs.song_name, songs.length FROM albums JOIN songs ON albums.id = songs.album_id;

--d. Join the 'artist' and 'albums' tables
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE "%a%";

--e. Sort the albums in Z-A order
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

--f. Join the 'albums' and 'songs' tables [Z-A]
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY albums.album_title DESC;