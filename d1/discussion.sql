[1] SECTION Add new records
--Add 5 artists, 2 albums each, 2 songs per album

--5 new artists
INSERT INTO artists (name) VALUES 
("Taylor Swift"),
("Lady Gaga"),
("Justin Bieber"),
("Ariana Grande"),
("Bruno Mars");

INSERT INTO albums (album_title, date_released, artist_id) VALUES
("Fearless", "2008-1-1", 3),
("Red", "2012-1-1", 3);

--2 songs for fearless album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Fearless",246, "Pop rock", 6),
("State of Grace", 213, "Rock", 6);

--2 songs for red album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Love Story",312, "Country pop", 7),
("Begin Again", 304, "Country", 7);

--2 albums for Lady G.
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("A Star is Born", "2018-1-1", 4),
("Born This Way", "2011-1-1", 4);

--2 songs for A Star is Born album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Shallow",250, "Country, rock, folk rock", 10),
("Always Remember Us this Way", 310, "Country", 10);

--2 songs for Born this Way album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Bad Romance",320, "Electro, pop", 11),
("Paparazzi", 258, "Pop", 11);


--Justin B. 1 song per album
--2 albums for Justin B.
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("Purpose", "2015-1-1", 5),
("Believe", "2012-1-1", 5);

--1 song for Purpose album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Sorry",242, "Dancehall-poptropical", 12);

--1 song for Believe album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Boyfriend",320, "Pop", 13);


--Ariana 1 song per album
--2 albums for Ariana G.
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("Dangerous Woman", "2016-1-1", 6),
("Thank U, Next", "2019-1-1", 6);

--1 song for Dangerous Woman album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Into You",320, "EDM house", 14);

--1 song for Thank U, Next album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Thank U, Next",246, "Pop, R&B", 15);


--Bruno 1 song per album
--2 albums for Bruno M
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("24K Magic", "2016-1-1", 7),
("Earth to Mars", "2011-1-1", 7);

--1 song for 24k Magic album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("24K Magic",400, "Funk, Disco, R&B", 16);

--1 song for Earth to Mars album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Lost",250, "Pop", 17);


[2] SECTION - Advanced Selection

SELECT * FROM songs WHERE id = 11;
--exclude from the records
SELECT * FROM songs WHERE id != 11;

--Greater than or equal
SELECT * FROM songs WHERE id < 11;
SELECT * FROM songs WHERE id <= 11;
SELECT * FROM songs WHERE id > 11;
SELECT * FROM songs WHERE id >= 11;

--Get specific ID (OR)
SELECT * FROM songs WHERE id = 1 OR id = 3 OR id = 5;

--Get specific IDs (IN)
SELECT * FROM songs WHERE id IN (1,3,5);
SELECT * FROM songs WHERE genre IN ("Pop", "K-pop");

--Combining conditions
SELECT * FROM songs WHERE album_id = 5 AND id < 3;

--Find Partial Mathces
SELECT * FROM songs WHERE song_name LIKE "%e"; --select keyword from the end
SELECT * FROM songs WHERE song_name LIKE "b%";
SELECT * FROM songs WHERE song_name LIKE "ba%";
SELECT * FROM songs WHERE song_name LIKE "%e%";

SELECT * FROM albums WHERE date_released LIKE "20_8-01-01";

--sorting Keywords
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

--getting distinct records
SELECT DISTINCT genre FROM songs;

[3] Table Joins
--Combine artist table and albums table
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

--More than 2 tables
SELECT * FROM artists
JOIN albums ON artists.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id;

SELECT artists.name, albums.album_title, songs.song_name 
FROM artists 
JOIN albums ON artists.id = albums.artist_id
JOIN songs on songs.album_id = albums.id;

SELECT * FROM artists
LEFT JOIN albums ON artists.id = albums.artist_id